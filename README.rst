===============
OpenStack Tools
===============

This repository includes useful utilties needed for operations of OpenStack,
however, it's important to note that the existance of some of these tools are
a bug.  These are simple interim solutions (especially clean-up tools) which
should all be non-existant as they should be fixed in upstream.

These tools are also published on DockerHub under ``vexxhost/openstack-tools``.
