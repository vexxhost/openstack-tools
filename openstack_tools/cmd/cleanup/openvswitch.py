# Copyright 2020 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Clean-up stale Open vSwitch ports

It is possible that for some reason, when cleaning up system interfaces, that
Open vSwitch ends up with many ports inside it's configuration which don't
actually exist on the physical host anymore.  This script removes them all to
reduce the size of the Open vSwitch database.

NOTE(mnaser): This tool should simply not exist.  Anything in here should
              only be here because we're in the process of figuring out why
              we need to clean it up.  The fact that this file currently
              exists is a bug.
"""

import argparse

import ovs.db.idl


def main():
    """Entry-point for script."""

    parser = argparse.ArgumentParser(description='Open vSwitch Clean-up tool')
    parser.add_argument('--remote', default='unix:/run/openvswitch/db.sock')
    parser.add_argument('--schema',
                        default='/usr/share/openvswitch/vswitch.ovsschema')
    parser.add_argument('--apply', action='store_true')

    args = parser.parse_args()

    schema_helper = ovs.db.idl.SchemaHelper(args.schema)
    schema_helper.register_columns("Open_vSwitch", ["bridges"])
    schema_helper.register_columns("Bridge", ["name", "ports"])
    schema_helper.register_columns("Port", ["name", "interfaces"])
    schema_helper.register_columns("Interface", [])

    idl = ovs.db.idl.Idl(args.remote, schema_helper)

    seq_no = idl.change_seqno
    while True:
        idl.run()

        if seq_no == idl.change_seqno:
            poller = ovs.poller.Poller()
            idl.wait(poller)
            poller.block()
            continue

        seq_no = idl.change_seqno
        break

    for _, bridge in idl.tables["Bridge"].rows.items():
        ports = []

        for port in bridge.ports:
            assert len(port.interfaces) == 1
            interface = port.interfaces[0]

            if interface.ofport == [-1]:
                print("%s is invalid" % interface.name)
                continue
            ports.append(port)

        txn = ovs.db.idl.Transaction(idl)
        bridge.ports = ports
        txn.commit_block()
